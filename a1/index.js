// find users with letter s in their first name or d in their last name.
		db.users.find({
		$or: [
		{"firstName": {$regex: "s", $options: "i"}},
		{"lastName": {$regex: "d", $options: "i"}}
		]
		},
		{
			"firstName": 1,
			"lastName": 1,
			"_id": 0
		}
		);

// find users who are from the hr department and their age is greater than or equal to 70
// a. use $and 
		db.users.find(
		{
				$and: [
					{ $or: [
							{age: {$gt:70}},
							{age: {$eq:70}}
							]},
					{department: "HR"}
				]
		});

// find users with the letter e in their firstname and has an age of less than or equal to 30
// use $and, $regex, $lte
		db.users.find(
		{
				$and: [
					{ $or: [
							{age: {$lte:30}},
							{age: {$eq:30}}
							]},
					{firstName: {$regex: "e", $options: 'i'}
				}
				]
		});
// end of activity 26